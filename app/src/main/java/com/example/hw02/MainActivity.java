package com.example.hw02;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class MainActivity extends AppCompatActivity {

    private static final int NOTIFICATION_ID = 10;
    static MainActivity single;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        single = this;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu ; this adds items to the action bar if it is present .
        getMenuInflater().inflate(R.menu.menu , menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.web:
                // User chose the "Settings" item, show the app settings UI...
                Log.i("sf", "f");
                TextView data_view = findViewById(R.id.data_view);

                String url = "https://api.myip.com/";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

                GetAWebResourceTask myTask = new GetAWebResourceTask(restTemplate, data_view);
                myTask.execute(url);
                data_view.setText("Loading...");
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void show_notification(){
        String channelid = " my_channel ";
        NotificationManager manager = (NotificationManager) getSystemService (
                NOTIFICATION_SERVICE );
        NotificationChannel ch = new NotificationChannel(channelid, "Howdy",
                NotificationManager.IMPORTANCE_DEFAULT);
        manager.createNotificationChannel(ch);

        Notification n = new Notification.Builder(this, channelid)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Request Result")
                .setContentText("HTTP Request results are ready")
                .setAutoCancel(true)
                .build();

        manager.notify(NOTIFICATION_ID, n);
    }
}
