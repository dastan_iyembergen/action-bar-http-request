package com.example.hw02;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class GetAWebResourceTask extends AsyncTask< String, Void, String > {
    final public static String TAG = "GetAWebResourceTask";
    private RestTemplate restTemplate ;
    @SuppressLint("StaticFieldLeak")
    private TextView textView;

    public GetAWebResourceTask(RestTemplate restTemplate, TextView textView) {
        this.restTemplate = restTemplate ;
        this.textView = textView ;
    }

    @Override
    protected String doInBackground(String ...params) {
        String ip_json = restTemplate.getForObject(params [0], String.class, "Android");
        Log.d(TAG, ip_json);

        String ip;
        try {
            JSONObject jObject = new JSONObject(ip_json);
            ip = jObject.getString("ip");
        } catch (JSONException e) {
            e.printStackTrace();
            return "Could not find your IP.";
        }

        String url = "https://weatherstack.com/ws_api.php?ip=" + ip;
        RestTemplate restTemplate2 = new RestTemplate();
        restTemplate2.getMessageConverters().add(new StringHttpMessageConverter());

        String result = restTemplate.getForObject(url, String.class, "Android");
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        String temp;
        String region;
        String description;
        String ip;
        try {
            JSONObject jObject = new JSONObject(s);
            ip = jObject.getJSONObject("request").getString("query");
            region = jObject.getJSONObject("location").getString("region");
            temp = jObject.getJSONObject("current").getString("temperature");
            description = jObject.getJSONObject("current").getString("weather_descriptions");
        } catch (JSONException e) {
            e.printStackTrace();
            textView.setText("Error in getting data.");
            return;
        }
        MainActivity.single.show_notification();
        textView.setText("IP: "+ ip + "\n" +
                "region: " + region + "\n" +
                "temperature: " + temp + " ^C\n" +
                "Description: " + description);
    }
}
